const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

// For checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// For user registration
router.post("/register", userControllers.registerUser);

router.post("/login", userControllers.loginUser);

router.get("/details/",auth.verify,  userControllers.getProfile);

// Route for enrolling a user
router.post("/enroll", auth.verify, userControllers.enroll)

module.exports = router;