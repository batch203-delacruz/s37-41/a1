const express = require("express");
const auth = require("../auth");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

console.log(courseControllers);

// Routes for Creating course
router.post("/", auth.verify, courseControllers.addCourse);


// Routes for viewing courses(admin only)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// Routes for viewing active courses(anyone)
router.get("/", courseControllers.getAllActiveCourses);

// Routes for viewing a specific course
router.get("/:courseId", courseControllers.getCourse);


// Route for updating a course
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// Route for archiving a course
router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);


module.exports = router;